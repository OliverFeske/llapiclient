﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class Client : MonoBehaviour
{
	public static Client Instance { private set; get; }

	private const int MAX_USER = 100;
	private const int PORT = 26000;
	private const int WEB_PORT = 26001;
	private const int BYTE_SIZE = 1024;
	private const string SERVER_IP = "192.168.1.225";           // "localhost" did not work
	//private const string SERVER_IP = "192.168.2.189";           // "localhost" did not work
	//private const string SERVER_IP = "192.168.2.105";           // home

	// basically an int, but smaller
	private byte reliableChannel;
	private byte error;
	private int connectionId;
	private int hostId;

	public Account self;
	private string token;
	private bool isStarted = false;

	#region MonoBehaviour
	void Start()
	{
		Instance = this;
		DontDestroyOnLoad(gameObject);
		Init();
	}
	void Update()
	{
		UpdateMessagePump();
	}
	#endregion

	public void Init()
	{
		NetworkTransport.Init();

		ConnectionConfig cc = new ConnectionConfig();
		reliableChannel = cc.AddChannel(QosType.Reliable);

		HostTopology topo = new HostTopology(cc, MAX_USER);

		// Client only Code
		hostId = NetworkTransport.AddHost(topo, 0);                                 // 0 means, that it is closed for everybody exept the server

#if UNITY_WEBGL && !UNITY_EDITOR
		// Web Client
		connectionId = NetworkTransport.Connect(hostId, SERVER_IP, WEB_PORT, 0, out error);
		Debug.Log($"Connecting from Web");
#else
		// Standalone Client
		connectionId = NetworkTransport.Connect(hostId, SERVER_IP, PORT, 0, out error);            // Error Codes https://docs.unity3d.com/ScriptReference/Networking.NetworkError.html
		Debug.Log("Connecting from standalone");
#endif
		Debug.Log($"Attempting to connect on {SERVER_IP}...");
		isStarted = true;
	}

	public void Shutdown()
	{
		isStarted = false;
		NetworkTransport.Shutdown();
	}

	public void UpdateMessagePump()
	{
		if (!isStarted) { return; }

		int recHostId;              // Which platform is sending this?
		int connectionId;           // Which user is sending this? 
		int channelId;              // Which lane is he sending this from?

		byte[] recBuffer = new byte[BYTE_SIZE];         // Array that stores the messages (receiving buffer)
		int dataSize;                                   // Size of the message

		// If we have a message the following information get filled
		NetworkEventType type = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, BYTE_SIZE, out dataSize, out error);
		switch (type)
		{
			case NetworkEventType.Nothing:
				break;

			case NetworkEventType.ConnectEvent:
				Debug.Log("We have connected to the server!");
				break;

			case NetworkEventType.DisconnectEvent:
				Debug.Log("We have been disconnected!");
				break;

			case NetworkEventType.DataEvent:
				BinaryFormatter formatter = new BinaryFormatter();
				MemoryStream ms = new MemoryStream(recBuffer);
				NetMsg msg = (NetMsg)formatter.Deserialize(ms);

				OnData(connectionId, channelId, recHostId, msg);
				break;

			default:
			case NetworkEventType.BroadcastEvent:
				Debug.Log("Unexpected network event type");
				break;
		}
	}

	#region OnData
	void OnData(int cnnId, int channelId, int recHostId, NetMsg msg)
	{
		switch (msg.OP)
		{
			case NetOP.None:
				Debug.Log("Unexpected NETOP");
				break;

			case NetOP.OnCreateAccount:
				OnCreateAccount((Net_OnCreateAccount)msg);
				break;

			case NetOP.OnLoginRequest:
				OnLoginRequest((Net_OnLoginRequest)msg);
				break;
		}
	}

	void OnCreateAccount(Net_OnCreateAccount oca)
	{
		LobbyScene.Instance.EnableInputs();
		LobbyScene.Instance.ChangeAuthenticationMessage(oca.Information);
	}
	void OnLoginRequest(Net_OnLoginRequest olr)
	{
		LobbyScene.Instance.ChangeAuthenticationMessage(olr.Information);

		// Unable to login
		if (olr.Success != 1)
		{
			LobbyScene.Instance.EnableInputs();
		}
		else
		{
			// Successfull login
			// This is where we are going to save data about ourself
			self = new Account();
			self.ActiveConnection = olr.ConnectionId;
			self.Username = olr.Username;
			self.Discriminator = olr.Discriminator;

			token = olr.Token;

			SceneManager.LoadScene("Hub");
		}
	}
	#endregion

	#region Send
	public void SendServer(NetMsg msg)
	{
		// This is where we hold our data
		byte[] buffer = new byte[BYTE_SIZE];

		// This is where you would crush your data into a byte[]
		BinaryFormatter formatter = new BinaryFormatter();
		MemoryStream ms = new MemoryStream(buffer);
		formatter.Serialize(ms, msg);

		NetworkTransport.Send(hostId, connectionId, reliableChannel, buffer, BYTE_SIZE, out error);
	}
	public void SendCreateAccount(string username, string password, string email)
	{
		if (!Utility.IsUsername(username))
		{
			// Invalid username
			LobbyScene.Instance.ChangeAuthenticationMessage("Username is invalid");
			LobbyScene.Instance.EnableInputs();
			return;
		}
		if (password == null || password == "")
		{
			// Invalid password
			LobbyScene.Instance.ChangeAuthenticationMessage("Password is empty");
			LobbyScene.Instance.EnableInputs();
			return;
		}
		if (!Utility.IsEmail(email))
		{
			// Invalid email
			LobbyScene.Instance.ChangeAuthenticationMessage("Email is invalid");
			LobbyScene.Instance.EnableInputs();
			return;
		}

		Net_CreateAccount ca = new Net_CreateAccount();

		ca.Username = username;
		ca.Password = Utility.Sha256FromString(password);
		ca.Email = email;

		LobbyScene.Instance.ChangeAuthenticationMessage("Sending Request...");
		SendServer(ca);
	}
	public void SendLoginRequest(string usernameOrEmail, string password)
	{
		if (!Utility.IsUsernameAndDiscriminator(usernameOrEmail) && !Utility.IsEmail(usernameOrEmail))
		{
			// Invalid username or email
			LobbyScene.Instance.ChangeAuthenticationMessage("Email or Username#Discriminator is invalid");
			LobbyScene.Instance.EnableInputs();
			return;
		}
		if (password == null || password == "")
		{
			// Invalid password
			LobbyScene.Instance.ChangeAuthenticationMessage("Password is empty");
			LobbyScene.Instance.EnableInputs();
			return;
		}

		Net_LoginRequest ca = new Net_LoginRequest();

		ca.UsernameOrEmail = usernameOrEmail;
		ca.Password = Utility.Sha256FromString(password);

		LobbyScene.Instance.ChangeAuthenticationMessage("Sending login Request...");

		SendServer(ca);
	}

	public void SendAddFollow(string usernameOrEmail)
	{
		Net_AddFollow af = new Net_AddFollow();

		af.Token = token;
		af.UsernameDiscriminatorOrEmail = usernameOrEmail;

		SendServer(af);
	}
	public void SendRemoveFollow(string username)
	{
		Net_RemoveFollow rf = new Net_RemoveFollow();

		rf.Token = token;
		rf.UsernameDiscriminat = username;

		SendServer(rf);
	}
	public void SendRequestFollow()
	{
		Net_RequestFollow rf =new Net_RequestFollow();

		rf.Token = token;

		SendServer(rf);
	}
	#endregion
}
